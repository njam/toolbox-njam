use std::io;
use std::sync::{Arc, Mutex};

use log::LevelFilter;
use simplelog::{CombinedLogger, Config, TerminalMode, TermLogger, WriteLogger, ConfigBuilder};

pub struct Logger {
    memory: LoggerMemory,
}

impl Logger {
    pub fn new(log_level: LevelFilter) -> Self {
        let term_config = ConfigBuilder::new()
            .set_max_level(LevelFilter::Off)
            .set_time_level(LevelFilter::Off)
            .set_thread_level(LevelFilter::Off)
            .build();
        let memory = LoggerMemory::default();
        CombinedLogger::init(
            vec![
                TermLogger::new(log_level, term_config, TerminalMode::Stdout),
                WriteLogger::new(LevelFilter::Debug, Config::default(), memory.clone()),
            ]
        ).unwrap();
        Self {
            memory,
        }
    }

    /// Returns all log messages since the beginning
    pub fn lines(&self) -> String {
        let bytes = self.memory.bytes.lock().unwrap();
        String::from_utf8_lossy(&bytes).to_string()
    }
}


#[derive(Default, Clone)]
struct LoggerMemory {
    bytes: Arc<Mutex<Vec<u8>>>,
}

impl io::Write for LoggerMemory {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.bytes.lock().unwrap()
            .write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.bytes.lock().unwrap()
            .flush()
    }
}
