#![allow(
clippy::ptr_arg,
clippy::collapsible_else_if,
)]


use log::LevelFilter;
use structopt::StructOpt;

use crate::config::*;
use crate::container_commands::InternalCommands;
use crate::logger::Logger;
use crate::toolbox::*;

pub mod podman;
pub mod toolbox;
pub mod toolbox_containers;
pub mod command;
pub mod logger;
pub mod container_commands;
pub mod config;
pub mod util;
pub mod create_user;

#[derive(StructOpt)]
struct Opt {
    /// Path to config file.
    #[structopt(short = "c", long = "config", default_value)]
    config: ConfigPath,
    /// Print debug messages
    #[structopt(short = "v", long = "verbose")]
    verbose: bool,
    /// Only print warning messages
    #[structopt(short = "q", long = "quiet")]
    quiet: bool,
    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(StructOpt)]
enum Command {
    #[structopt(flatten)]
    Public(PublicCommands),
    /// Internally used commands (running in the container)
    #[structopt(setting = clap::AppSettings::Hidden)]
    Internal(InternalCommands),
}

#[derive(StructOpt)]
enum PublicCommands {
    /// List all toolboxes
    #[structopt(alias = "ls")]
    List,
    /// Start a shell in a toolbox
    Shell(CommandShell),
    /// Run a command in a toolbox
    #[structopt(setting = clap::AppSettings::TrailingVarArg)]
    #[structopt(setting = clap::AppSettings::AllowLeadingHyphen)]
    Exec(CommandExec),
    /// Delete a toolbox
    #[structopt(aliases = & ["del", "rm"])]
    Delete(CommandDelete),
}

#[derive(StructOpt)]
struct CommandShell {
    /// Toolbox name
    name: String,
}

#[derive(StructOpt)]
struct CommandExec {
    /// Toolbox name
    name: String,
    /// Command to execute
    command: String,
    /// Arguments
    arg: Vec<String>,
}

#[derive(StructOpt)]
struct CommandDelete {
    /// Toolbox name
    name: String,
    /// Silently ignore if the toolbox by the given name does not exist
    #[structopt(long = "if-exists")]
    if_exists: bool,
}

fn main() {
    let opt: Opt = Opt::from_args();
    let log_level = if opt.verbose {
        LevelFilter::Debug
    } else if opt.quiet {
        LevelFilter::Warn
    } else {
        LevelFilter::Info
    };
    let logger = Logger::new(log_level);
    let result = try_run(opt.config, opt.cmd);
    if let Err(error) = result {
        // Get the first and last error of the error chain
        let errors_len = error.chain().count();
        let errors: Vec<String> = error.chain()
            .enumerate()
            .filter(|(i, _)| *i == 0 || *i == errors_len - 1)
            .map(|(_, e)| e.to_string())
            .collect();
        eprintln!("ERROR: {}", errors.join(" - "));
        if !opt.verbose {
            eprint!("--- Event Log ---\n{}", logger.lines());
        }
        std::process::exit(1);
    }
}

fn try_run(config_path: ConfigPath, command: Command) -> Result<(), anyhow::Error> {
    match command {
        Command::Public(command) => {
            let config = read_config(config_path)?;
            let toolbox = Toolbox::new(config)?;
            match command {
                PublicCommands::List => {
                    toolbox.list()
                }
                PublicCommands::Shell(opt) => {
                    toolbox.attach_shell(&opt.name)
                }
                PublicCommands::Exec(opt) => {
                    toolbox.exec(&opt.name, opt.command, opt.arg)
                }
                PublicCommands::Delete(opt) => {
                    toolbox.delete(&opt.name, opt.if_exists)
                }
            }
        }
        Command::Internal(command) => {
            match command {
                InternalCommands::SetupContainer { payload } => {
                    container_commands::setup_container(payload)
                }
                InternalCommands::StartShell { payload } => {
                    container_commands::start_shell(payload)
                }
                InternalCommands::RunPid1 => {
                    container_commands::run_pid1()
                }
            }
        }
    }
}
