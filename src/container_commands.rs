use std::path::PathBuf;
use std::str::FromStr;
use std::time::Duration;

use anyhow::Context;
use fs_err as fs;
use serde::{Deserialize, Serialize};
use structopt::StructOpt;
use users::os::unix::UserExt;

use crate::command::*;
use crate::create_user::*;
use crate::util::current_user::*;

#[derive(StructOpt)]
pub enum InternalCommands {
    /// INTERNAL: configure a toolbox container
    SetupContainer {
        #[structopt(long = "payload")]
        payload: CommandInternalSetupContainer,
    },
    /// INTERNAL: start shell in a toolbox container
    StartShell {
        #[structopt(long = "payload")]
        payload: CommandInternalStartShell,
    },
    /// INTERNAL: run a pid1 forever
    RunPid1,
}

#[derive(Serialize, Deserialize)]
pub struct CommandInternalSetupContainer {
    pub user_uid: u32,
    pub user_gid: u32,
    pub user_name: String,
    pub home_dir: PathBuf,
    pub shell_path: Option<PathBuf>,
    pub toolbox_name: String,
    pub mounts: Vec<PathBuf>,
}

impl FromStr for CommandInternalSetupContainer {
    type Err = serde_json::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        serde_json::from_str(s)
    }
}

#[derive(Serialize, Deserialize)]
pub struct CommandInternalStartShell {
    pub shell_path: Option<PathBuf>,
}

impl FromStr for CommandInternalStartShell {
    type Err = serde_json::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        serde_json::from_str(s)
    }
}

/// Prepare a container to be used as a toolbox.
/// This function is executed once after the container is created, and runs inside the container.
/// It should be highly portable, so it can run in a variety of Linux container images. Therefore
/// it does not execute external programs like "adduser".
pub fn setup_container(opt: CommandInternalSetupContainer) -> Result<(), anyhow::Error> {
    // Setup group
    if opt.user_gid != 0 {
        ensure_group(
            opt.user_gid,
            &opt.user_name,
            &opt.user_name,
        )?;
    }

    // Setup user
    if opt.user_uid != 0 {
        ensure_user(
            &opt.user_name,
            opt.user_uid,
            opt.user_gid,
            &opt.home_dir.to_string_lossy(),
            &opt.shell_path.map_or("".to_string(), |p| p.to_string_lossy().to_string()),
        )?;
        configure_sudo(&opt.user_name)?;
    }

    // Setup home
    ensure_home(
        &opt.home_dir,
        opt.user_uid,
        opt.user_gid,
        &opt.mounts,
    )?;
    fix_home_sub_dirs(
        &opt.home_dir,
        opt.user_uid,
        opt.user_gid,
        &opt.mounts,
    )?;

    // Create file in "/run" to indicate that this is a toolbox
    let run_env_path = "/run/.toolbox-njam-env";
    fs::write(run_env_path, format!("TOOLBOX_NAME={}\n", opt.toolbox_name))
        .context(format!("Cannot write run-env file to '{}'", run_env_path))?;

    Ok(())
}

pub fn start_shell(opt: CommandInternalStartShell) -> Result<(), anyhow::Error> {
    let shell_path = match opt.shell_path {
        Some(shell_path) => shell_path,
        None => {
            let user = get_current_user()?;
            let shell_path = user.shell();
            if !shell_path.to_string_lossy().is_empty() {
                shell_path.to_path_buf()
            } else {
                PathBuf::from("/bin/sh")
            }
        }
    };
    let error = exec_command(shell_path, &[] as &[&str]);
    Err(error)
}

/// Reap child processes, and handle termination signal
///
/// From https://github.com/cgwalters/coretoolbox/blob/04e36894cdb912cd4d4c91b26436c57a2d96707d/src/coretoolbox.rs#L688
pub fn run_pid1() -> Result<(), anyhow::Error> {
    unsafe {
        signal_hook::register(signal_hook::SIGCHLD, || {
            use nix::sys::wait::{waitpid, WaitStatus, WaitPidFlag};
            while let Ok(status) = waitpid(None, Some(WaitPidFlag::WNOHANG)) {
                if let WaitStatus::StillAlive = status { break; }
            }
        })?;
        signal_hook::register(signal_hook::SIGTERM, || {
            std::process::exit(0)
        })?;
    };
    loop {
        std::thread::sleep(Duration::from_secs(std::u64::MAX));
    }
}
