use std::collections::{HashMap, HashSet};
use std::ffi::OsStr;
use std::path::PathBuf;

use anyhow::anyhow;
use itertools::Itertools;
use serde::Deserialize;

use crate::command::*;

pub type PodmanContainerId = String;

pub struct Podman {
    program: String,
}

impl Podman {
    pub fn new() -> Self {
        Self {
            program: "podman".into(),
        }
    }

    /// Run a command in a new container and return the container-id
    pub fn run_detach(&self, opt: PodmanRunDetach) -> Result<PodmanContainerId, anyhow::Error> {
        let output = self.run_podman_command(opt.to_args())?;
        let container_id = output.trim().to_string();
        Ok(container_id)
    }

    /// Execute a command in a running container
    pub fn exec(&self, opt: PodmanExec) -> Result<(), anyhow::Error> {
        self.run_podman_command(opt.to_args())?;
        Ok(())
    }

    /// Execute a command in a running container, and replace the current process with it
    pub fn exec_replace(&self, opt: PodmanExec) -> Result<(), anyhow::Error> {
        self.exec_podman_command(opt.to_args())?;
        Ok(())
    }

    /// List containers
    pub fn list_containers(&self, opt: PodmanList) -> Result<Vec<PodmanInspectReturn>, anyhow::Error> {
        let inspections: Vec<PodmanInspectReturn> = self.run_podman_command(opt.to_args())?
            .lines()
            .map(|id| {
                self.inspect_container(PodmanInspectContainer {
                    container: id.to_string(),
                })
            })
            .collect::<Result<_, _>>()?;
        Ok(inspections)
    }

    /// Start an existing container
    pub fn start(&self, opt: PodmanStart) -> Result<(), anyhow::Error> {
        self.run_podman_command(opt.to_args())?;
        Ok(())
    }

    /// Remove an existing container
    pub fn rm(&self, opt: PodmanRemove) -> Result<(), anyhow::Error> {
        self.run_podman_command(opt.to_args())?;
        Ok(())
    }

    pub fn inspect_container(&self, opt: PodmanInspectContainer) -> Result<PodmanInspectReturn, anyhow::Error> {
        let output = self.run_podman_command(opt.to_args())?;
        let result: Vec<PodmanInspectReturn> = serde_json::from_str(&output)?;
        match result.into_iter().exactly_one() {
            Ok(item) => Ok(item),
            Err(err) => {
                Err(anyhow!("Expected 1, but got {} outputs when inspecting container.", err.count()))
            }
        }
    }

    fn run_podman_command<I, S>(&self, args: I) -> Result<String, anyhow::Error>
        where
            I: IntoIterator<Item=S> + Clone,
            S: AsRef<OsStr>,
    {
        run_command(&self.program, args)
    }

    fn exec_podman_command<I, S>(&self, args: I) -> Result<String, anyhow::Error>
        where
            I: IntoIterator<Item=S> + Clone,
            S: AsRef<OsStr>,
    {
        Err(exec_command(&self.program, args))
    }
}

impl Default for Podman {
    fn default() -> Self {
        Self::new()
    }
}


// PodmanStart

#[derive(Debug)]
pub struct PodmanStart {
    pub container: PodmanContainerId,
}

impl PodmanStart {
    fn to_args(&self) -> Vec<String> {
        vec!(
            "start".to_string(),
            self.container.to_string(),
        )
    }
}


// PodmanRemove

#[derive(Debug)]
pub struct PodmanRemove {
    pub container: PodmanContainerId,
    pub force: bool,
}

impl PodmanRemove {
    fn to_args(&self) -> Vec<String> {
        let mut args = vec!(
            "rm".to_string(),
        );
        if self.force {
            args.push("--force".into());
        }
        args.push(self.container.to_string());
        args
    }
}


// PodmanExec

#[derive(Debug)]
pub struct PodmanExec {
    pub container: PodmanContainerId,
    pub interactive: bool,
    pub tty: bool,
    pub user: String,
    pub workdir: Option<PathBuf>,
    pub command: PodmanCommand,
}

impl PodmanExec {
    fn to_args(&self) -> Vec<String> {
        let mut args = vec!(
            "exec".to_string(),
            format!("--user={}", &self.user),
        );
        if self.interactive {
            args.push("--interactive".into());
        }
        if self.tty {
            args.push("--tty".into());
        }
        if let Some(workdir) = &self.workdir {
            args.push(format!("--workdir={}", workdir.to_string_lossy()));
        }
        args.push(self.container.to_string());
        args.extend(self.command.to_args().into_iter());
        args
    }
}


// PodmanPs

#[derive(Debug)]
pub struct PodmanList {
    pub all: bool,
    pub filter: Option<String>,
}

impl PodmanList {
    fn to_args(&self) -> Vec<String> {
        let mut args = vec!(
            "ps".into(),
            "--format={{.ID}}".into(),
        );
        if self.all {
            args.push("--all".into());
        }
        if let Some(filter) = &self.filter {
            args.push(format!("--filter={}", filter));
        }
        args
    }
}


// PodmanInspectContainer

#[derive(Debug)]
pub struct PodmanInspectContainer {
    pub container: PodmanContainerId,
}

impl PodmanInspectContainer {
    fn to_args(&self) -> Vec<String> {
        vec!(
            "container".into(),
            "inspect".into(),
            self.container.to_string(),
        )
    }
}


// PodmanInspectContainerReturn

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct PodmanInspectReturn {
    pub id: String,
    pub image_name: String,
    pub name: String,
    pub state: PodmanInspectReturnState,
    pub config: PodmanInspectReturnConfig,
}

impl PodmanInspectReturn {
    pub fn label(&self, name: &str) -> Result<&String, anyhow::Error> {
        self.config.labels.get(name)
            .ok_or_else(|| anyhow!("Container '{}' has no label '{}'.", self.id, name))
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct PodmanInspectReturnState {
    pub running: bool,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct PodmanInspectReturnConfig {
    pub labels: HashMap<String, String>,
}


// PodmanRun

#[derive(Debug)]
pub struct PodmanRunDetach {
    pub image: String,
    pub command: PodmanCommand,
    pub name: String,
    pub user: String,
    pub user_ns: PodmanUserNs,
    pub security_opt: HashSet<String>,
    pub cap_add: HashSet<String>,
    pub privileged: bool,
    pub mounts: Vec<PodmanMount>,
    pub devices: HashSet<PathBuf>,
    pub envs: Vec<PodmanEnvVar>,
    pub labels: Vec<(String, String)>,
}

impl PodmanRunDetach {
    fn to_args(&self) -> Vec<String> {
        let mut args = vec!(
            "run".into(),
            "--detach".into(),
            "--network=host".into(),
            format!("--name={}", self.name),
            format!("--user={}", self.user),
            self.user_ns.to_arg(),
        );
        for value in &self.security_opt {
            args.push(format!("--security-opt={}", value));
        }
        if self.privileged {
            args.push("--privileged".into());
        } else {
            for value in &self.cap_add {
                args.push(format!("--cap-add={}", value));
            }
        }
        for value in &self.devices {
            args.push(format!("--device={}", value.to_string_lossy()));
        }
        args.extend(self.mounts.iter().map(PodmanMount::to_arg));
        args.extend(self.envs.iter().map(PodmanEnvVar::to_arg));
        args.extend(self.labels.iter().map(|(key, value)| {
            format!("--label={}={}", key, value)
        }));
        args.push(self.image.clone());
        args.extend(self.command.to_args());
        args
    }
}


// PodmanCommand

#[derive(Debug)]
pub struct PodmanCommand {
    pub cmd: String,
    pub args: Vec<String>,
}

impl PodmanCommand {
    fn to_args(&self) -> Vec<String> {
        let mut args = vec!(self.cmd.clone());
        args.extend(self.args.clone().into_iter());
        args
    }
}


// PodmanMount

#[derive(Debug)]
pub enum PodmanMount {
    Bind {
        source: PathBuf,
        destination: PathBuf,
        read_only: bool,
    },
    Volume {
        source: PathBuf,
        destination: PathBuf,
        read_only: bool,
    },
    Tmpfs {
        destination: PathBuf,
    },
    DevPts {
        destination: PathBuf,
    },
}

impl PodmanMount {
    fn to_arg(&self) -> String {
        let value = match self {
            PodmanMount::Bind { source, destination, read_only } => {
                let mut value = format!("type=bind,source={},destination={}",
                                        source.to_string_lossy(),
                                        destination.to_string_lossy());
                if *read_only {
                    value.push_str(",ro=true");
                }
                value
            }
            PodmanMount::Volume { source, destination, read_only } => {
                let mut value = format!("type=volume,source={},destination={}",
                                        source.to_string_lossy(),
                                        destination.to_string_lossy());
                if *read_only {
                    value.push_str(",ro=true");
                }
                value
            }
            PodmanMount::Tmpfs { destination } => {
                format!("type=tmpfs,destination={}", destination.to_string_lossy())
            }
            PodmanMount::DevPts { destination } => {
                format!("type=devpts,destination={}", destination.to_string_lossy())
            }
        };
        format!("--mount={}", value)
    }
}


// PodmanEnvVar

#[derive(Debug)]
pub struct PodmanEnvVar {
    pub name: String,
    pub value: String,
}

impl PodmanEnvVar {
    pub fn new<N: Into<String>, V: Into<String>>(name: N, value: V) -> Self {
        Self {
            name: name.into(),
            value: value.into(),
        }
    }

    fn to_arg(&self) -> String {
        format!("--env={}={}", self.name, self.value)
    }
}


// PodmanUserNs

#[derive(Debug)]
pub enum PodmanUserNs {
    Root,
    KeepId,
}

impl PodmanUserNs {
    fn to_arg(&self) -> String {
        let value = match self {
            PodmanUserNs::Root => "",
            PodmanUserNs::KeepId => "keep-id",
        };
        format!("--userns={}", value)
    }
}
