use std::io::{stdout, Write};
use std::path::PathBuf;

use anyhow::anyhow;
use itertools::{EitherOrBoth};
use tabwriter::TabWriter;

use crate::config::*;
use crate::toolbox_containers::*;
use crate::util::fs::*;
use crate::util::join_iters::join_iters;

pub struct Toolbox {
    config: Config,
    containers: ToolboxContainers,
}

impl Toolbox {
    pub fn new(config: Config) -> Result<Self, anyhow::Error> {
        Ok(Self {
            config,
            containers: ToolboxContainers::new()?,
        })
    }

    pub fn attach_shell(&self, name: &str) -> Result<(), anyhow::Error> {
        let pair = self.find_or_create_container(name)?;
        self.containers.attach_shell(&pair)?;
        Ok(())
    }

    pub fn exec(&self, name: &str, cmd: String, args: Vec<String>) -> Result<(), anyhow::Error> {
        let pair = self.find_or_create_container(name)?;
        self.containers.exec(&pair, cmd, args)?;
        Ok(())
    }

    pub fn list(&self) -> Result<(), anyhow::Error> {
        let statuses = self.list_statuses()?;
        let output = statuses.into_iter()
            .map(|item| {
                let status = match item {
                    ToolboxStatus::Created { .. } => "Created",
                    ToolboxStatus::NotCreated { .. } => "Not Created",
                    ToolboxStatus::Unmanaged { .. } => "Unmanaged (missing from config file)",
                };
                format!("{}\t{}\t{}\n", item.name(), item.image(), status)
            })
            .collect::<Vec<_>>()
            .join("");
        let mut tw = TabWriter::new(stdout());
        writeln!(&mut tw, "NAME\tIMAGE\tSTATUS")?;
        write!(&mut tw, "{}", output)?;
        tw.flush()?;
        Ok(())
    }

    pub fn delete(&self, name: &str, if_exists: bool) -> Result<(), anyhow::Error> {
        match self.containers.find(name)? {
            None => {
                if if_exists {
                    log::info!("Toolbox '{}' does not exist, ignoring.", name);
                    Ok(())
                } else {
                    Err(anyhow!("Toolbox '{}' does not exist.", name))
                }
            }
            Some(container) => {
                self.containers.delete(&container)?;
                log::info!("Toolbox '{}' deleted.", name);
                Ok(())
            }
        }
    }

    fn get_definition(&self, name: &str) -> Result<ToolboxDefinition, anyhow::Error> {
        self.config.definition(name)
            .ok_or_else(|| anyhow!("No toolbox definition with name '{}'", name))
    }

    fn find_or_create_container(&self, name: &str) -> Result<ToolboxPair, anyhow::Error> {
        let definition = self.get_definition(name)?;
        let pair = match self.containers.find(name)? {
            Some(container) => {
                ToolboxPair { definition, container }
            }
            None => {
                log::info!("Creating toolbox '{}'...", name);
                self.containers.create(&definition)?
            }
        };
        Ok(pair)
    }

    fn list_statuses(&self) -> Result<Vec<ToolboxStatus>, anyhow::Error> {
        let definitions = self.config.definitions();
        let containers = self.containers.list()?;
        let statuses = join_iters(
            definitions,
            containers,
            |definition| definition.name.clone(),
            |container| container.name.clone(),
        )
            .map(|item| {
                match item {
                    EitherOrBoth::Both(definition, container) => {
                        ToolboxStatus::Created(ToolboxPair { definition, container })
                    }
                    EitherOrBoth::Left(definition) => {
                        ToolboxStatus::NotCreated { definition }
                    }
                    EitherOrBoth::Right(container) => {
                        ToolboxStatus::Unmanaged { container }
                    }
                }
            })
            .collect();
        Ok(statuses)
    }
}

#[derive(Debug, Clone)]
pub struct ToolboxDefinition {
    pub name: String,
    pub image: String,
    pub shell: Option<PathBuf>,
    pub workdir: Option<PathBuf>,
    pub mounts: Vec<PathFileOrDir>,
    pub enable_nested_podman: bool,
    pub privileged: bool,
}

#[derive(Debug)]
pub struct ToolboxPair {
    pub definition: ToolboxDefinition,
    pub container: ToolboxContainer,
}

#[derive(Debug)]
pub enum ToolboxStatus {
    Created(ToolboxPair),
    NotCreated { definition: ToolboxDefinition },
    Unmanaged { container: ToolboxContainer },
}

impl ToolboxStatus {
    pub fn name(&self) -> &str {
        match self {
            ToolboxStatus::Created(created) => &created.definition.name,
            ToolboxStatus::NotCreated { definition } => &definition.name,
            ToolboxStatus::Unmanaged { container } => &container.name,
        }
    }

    pub fn image(&self) -> &str {
        match self {
            ToolboxStatus::Created(created) => &created.container.image,
            ToolboxStatus::NotCreated { definition } => &definition.image,
            ToolboxStatus::Unmanaged { container } => &container.image,
        }
    }

    pub fn mounts(&self) -> Option<&Vec<PathFileOrDir>> {
        match self {
            ToolboxStatus::Created(created) => Some(&created.definition.mounts),
            ToolboxStatus::NotCreated { definition } => Some(&definition.mounts),
            ToolboxStatus::Unmanaged { .. } => None,
        }
    }
}
