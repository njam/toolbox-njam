use std::path::{Path, PathBuf};
use nix::unistd::FchownatFlags;
use anyhow::Context;
use nix::sys::stat::FchmodatFlags;
use std::fmt;
use std::fmt::Formatter;

/// Change the owner of a file, without following symlinks
pub fn fs_chown<P>(path: P, uid: u32, gid: u32) -> Result<(), anyhow::Error>
    where
        P: AsRef<Path>
{
    let path = path.as_ref();
    let uid_nix = nix::unistd::Uid::from_raw(uid);
    let gid_nix = nix::unistd::Gid::from_raw(gid);
    nix::unistd::fchownat(None, path, Some(uid_nix), Some(gid_nix), FchownatFlags::NoFollowSymlink)
        .context(format!("Failed to change ownership of '{}' to '{}:{}'", path.display(), uid, gid))?;
    Ok(())
}

/// Change the permissions of a file, without following symlinks
pub fn fs_chmod<P>(path: P, mode: u32) -> Result<(), anyhow::Error>
    where
        P: AsRef<Path>
{
    let path = path.as_ref();
    let mode_nix = nix::sys::stat::Mode::from_bits_truncate(mode);
    nix::sys::stat::fchmodat(None, path, mode_nix, FchmodatFlags::NoFollowSymlink)
        .context(format!("Failed to change permissions of '{}' to '{:o}'", path.display(), mode))?;
    Ok(())
}

/// Holds a path and whether it's a file or a directory
#[derive(Debug, Clone)]
pub struct PathFileOrDir {
    path: PathBuf,
    is_file: bool,
}

impl PathFileOrDir {
    pub fn new(path: PathBuf, is_file: bool) -> Self {
        Self {
            path,
            is_file,
        }
    }

    /// Returns the first directory up the parent chain
    pub fn first_directory(&self) -> Option<&Path> {
        if self.is_file {
            self.path.parent()
        } else {
            Some(self.path.as_ref())
        }
    }

    pub fn is_file(&self) -> bool {
        self.is_file
    }

    pub fn path(&self) -> &Path {
        &self.path
    }

    pub fn path_buf(&self) -> PathBuf {
        self.path.clone()
    }

    pub fn exists(&self) -> bool {
        self.path.exists()
    }
}

impl fmt::Display for PathFileOrDir {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.path.display())
    }
}
