use itertools::EitherOrBoth;
use itertools::Itertools;

/// Create an iterator that merges items from two iterators based on the return value of a compare function.
///
/// Based on [Itertools::merge_join_by](https://docs.rs/itertools/0.8.2/itertools/trait.Itertools.html#method.merge_join_by)
pub fn join_iters<I1, I2, F1, F2, CompareKey>(iter1: I1, iter2: I2, join_by1: F1, join_by2: F2)
                                              -> impl Iterator<Item=EitherOrBoth<I1::Item, I2::Item>> + Sized
    where
        I1: IntoIterator,
        I2: IntoIterator,
        CompareKey: Ord,
        F1: Fn(&I1::Item) -> CompareKey,
        F2: Fn(&I2::Item) -> CompareKey,
{
    let vec1_sorted = iter1.into_iter()
        .sorted_by_key(|item1| join_by1(item1));
    let vec2_sorted = iter2.into_iter()
        .sorted_by_key(|item2| join_by2(item2));
    vec1_sorted.merge_join_by(vec2_sorted, move |item1, item2| {
        let key1: CompareKey = join_by1(item1);
        let key2: CompareKey = join_by2(item2);
        key1.cmp(&key2)
    })
}


#[cfg(test)]
mod tests {
    use std::vec::Vec;

    use insta::assert_debug_snapshot;

    use super::*;

    #[test]
    fn first_left() {
        let list1: Vec<String> = vec!(
            "foo1".into(),
            "foo2".into(),
        );
        let list2: Vec<String> = vec!(
            "foo2".into(),
        );
        let result = join_iters(
            list1, list2,
            |item1| item1.clone(),
            |item2| item2.clone(),
        );
        assert_debug_snapshot!(result.collect::<Vec<_>>(), @r###"
       ⋮[
       ⋮    Left(
       ⋮        "foo1",
       ⋮    ),
       ⋮    Both(
       ⋮        "foo2",
       ⋮        "foo2",
       ⋮    ),
       ⋮]
        "###);
    }

    #[test]
    fn last_left() {
        let list1: Vec<String> = vec!(
            "foo1".into(),
            "foo2".into(),
        );
        let list2: Vec<String> = vec!(
            "foo1".into(),
        );
        let result = join_iters(
            list1, list2,
            |item1| item1.clone(),
            |item2| item2.clone(),
        );
        assert_debug_snapshot!(result.collect::<Vec<_>>(), @r###"
       ⋮[
       ⋮    Both(
       ⋮        "foo1",
       ⋮        "foo1",
       ⋮    ),
       ⋮    Left(
       ⋮        "foo2",
       ⋮    ),
       ⋮]
        "###);
    }

    #[test]
    fn option() {
        let list1: Vec<String> = vec!(
            "foo1".into(),
            "foo2".into(),
        );
        let list2: Vec<String> = vec!(
            "".into(),
            "foo1".into(),
        );
        let result = join_iters(
            list1, list2,
            |item1| Some(item1.clone()),
            |item2| if item2.is_empty() { None } else { Some(item2.clone()) },
        );
        assert_debug_snapshot!(result.collect::<Vec<_>>(), @r###"
       ⋮[
       ⋮    Right(
       ⋮        "",
       ⋮    ),
       ⋮    Both(
       ⋮        "foo1",
       ⋮        "foo1",
       ⋮    ),
       ⋮    Left(
       ⋮        "foo2",
       ⋮    ),
       ⋮]
        "###);
    }

    #[test]
    fn last_diff() {
        let list1: Vec<String> = vec!(
            "foo1".into(),
            "foo2".into(),
        );
        let list2: Vec<String> = vec!(
            "foo1".into(),
            "fooXXX".into(),
        );
        let result = join_iters(
            list1, list2,
            |item1| item1.clone(),
            |item2| item2.clone(),
        );
        assert_debug_snapshot!(result.collect::<Vec<_>>(), @r###"
       ⋮[
       ⋮    Both(
       ⋮        "foo1",
       ⋮        "foo1",
       ⋮    ),
       ⋮    Left(
       ⋮        "foo2",
       ⋮    ),
       ⋮    Right(
       ⋮        "fooXXX",
       ⋮    ),
       ⋮]
        "###);
    }
}
