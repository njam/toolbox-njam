use users::{get_current_uid, get_user_by_uid, User};

use anyhow::anyhow;

pub fn get_current_user() -> Result<User, anyhow::Error> {
    let user_id = get_current_uid();
    get_user_by_uid(user_id)
        .ok_or_else(|| anyhow!("Cannot retrieve information for current user '{}'", user_id))
}
