use std::env;

use anyhow::anyhow;

pub fn env_opt(name: &str) -> Result<Option<String>, anyhow::Error> {
    match env::var(name) {
        Ok(value) => Ok(Some(value)),
        Err(env::VarError::NotPresent) => Ok(None),
        Err(error) => Err(anyhow!("Cannot get environment variable '{}': {}", name, error)),
    }
}
