use std::{fmt, fs};
use std::collections::HashMap;
use std::path::PathBuf;
use std::str::FromStr;

use anyhow::Context;
use serde::Deserialize;

use crate::toolbox::*;
use crate::util::fs::*;

pub fn read_config(path: ConfigPath) -> Result<Config, anyhow::Error> {
    let text = fs::read_to_string(&path.0)
        .context(format!("Cannot read config file '{}'", &path))?;
    let config = toml::from_str(&text)
        .context(format!("Cannot parse config file '{}'", &path))?;
    Ok(config)
}


/// Path to the application config file
#[derive(Debug, Clone)]
pub struct ConfigPath(PathBuf);

impl Default for ConfigPath {
    fn default() -> Self {
        let config_home = xdg::BaseDirectories::new()
            .unwrap_or_else(|e| panic!("Cannot detect XDG directories for reading config file: {}", e))
            .get_config_home();
        let path = config_home.join("toolbox-njam.toml");
        ConfigPath(path)
    }
}

impl fmt::Display for ConfigPath {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0.to_string_lossy())
    }
}

impl FromStr for ConfigPath {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(ConfigPath(PathBuf::from(s)))
    }
}


/// The application configuration
#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    #[serde(default)]
    toolbox: HashMap<ConfigItemToolboxName, ConfigItemToolbox>,
}

impl Config {
    pub fn definition(&self, name: &str) -> Option<ToolboxDefinition> {
        let key = ConfigItemToolboxName(name.to_string());
        self.toolbox.get(&key).cloned()
            .map(|cont| cont.into_toolbox_definition(name.to_string()))
    }

    pub fn definitions(&self) -> Vec<ToolboxDefinition> {
        self.toolbox.clone().into_iter()
            .map(|(key, cont)| {
                cont.into_toolbox_definition(key.0)
            })
            .collect()
    }
}


/// A single toolbox-item in the config
#[derive(Deserialize, Debug, Clone)]
struct ConfigItemToolbox {
    image: String,
    shell: Option<ConfigItemAbsolutePath>,
    workdir: Option<ConfigItemLocalPath>,
    #[serde(default)]
    mounts: Vec<ConfigItemLocalPath>,
    #[serde(default)]
    enable_nested_podman: bool,
    #[serde(default)]
    privileged: bool,
}

impl ConfigItemToolbox {
    fn into_toolbox_definition(self, name: String) -> ToolboxDefinition {
        ToolboxDefinition {
            name,
            image: self.image,
            shell: self.shell.map(|o| o.0),
            workdir: self.workdir.map(|o| o.canonical.path_buf()),
            mounts: self.mounts.into_iter().map(|p| p.canonical).collect(),
            enable_nested_podman: self.enable_nested_podman,
            privileged: self.privileged,
        }
    }
}


/// A toolbox container name in the config.
/// Checks that the name is valid using a regexp.
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
struct ConfigItemToolboxName(String);

impl<'de> Deserialize<'de> for ConfigItemToolboxName {
    fn deserialize<D>(deserializer: D) -> Result<ConfigItemToolboxName, D::Error>
        where D: serde::de::Deserializer<'de>,
    {
        struct Visitor;

        impl<'de> serde::de::Visitor<'de> for Visitor {
            type Value = ConfigItemToolboxName;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("A file path")
            }

            fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
                where E: serde::de::Error
            {
                let pattern = "^[a-zA-Z0-9_.-]+$";
                let re = regex::Regex::new(pattern).unwrap();
                if !re.is_match(value) {
                    return Err(E::custom(format!("Container name '{}' is invalid, must match pattern '{}'.", value, pattern)));
                }
                Ok(ConfigItemToolboxName(value.to_string()))
            }
        }

        deserializer.deserialize_str(Visitor)
    }
}


/// A file path in the config.
/// The 'canonical' form has `~` expanded.
#[derive(Debug, Clone)]
struct ConfigItemLocalPath {
    canonical: PathFileOrDir,
}

impl<'de> Deserialize<'de> for ConfigItemLocalPath {
    fn deserialize<D>(deserializer: D) -> Result<ConfigItemLocalPath, D::Error>
        where D: serde::de::Deserializer<'de>,
    {
        struct Visitor;

        impl<'de> serde::de::Visitor<'de> for Visitor {
            type Value = ConfigItemLocalPath;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("A local file path")
            }

            fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
                where E: serde::de::Error
            {
                let path = PathBuf::from(shellexpand::tilde(value).as_ref());
                let is_file = if path.exists() {
                    path.is_file()
                } else {
                    !value.ends_with('/')
                };
                Ok(ConfigItemLocalPath {
                    canonical: PathFileOrDir::new(path, is_file),
                })
            }
        }

        deserializer.deserialize_str(Visitor)
    }
}


/// An absolute path in the config file.
/// The path does not need to exist.
#[derive(Debug, Clone)]
struct ConfigItemAbsolutePath(PathBuf);

impl<'de> Deserialize<'de> for ConfigItemAbsolutePath {
    fn deserialize<D>(deserializer: D) -> Result<ConfigItemAbsolutePath, D::Error>
        where D: serde::de::Deserializer<'de>,
    {
        struct Visitor;

        impl<'de> serde::de::Visitor<'de> for Visitor {
            type Value = ConfigItemAbsolutePath;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("An absolute file path")
            }

            fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
                where E: serde::de::Error
            {
                let path = PathBuf::from(value);
                if !path.is_absolute() {
                    return Err(E::custom(format!("Path '{}' is invalid, must be absolute.", value)));
                }
                Ok(ConfigItemAbsolutePath(path))
            }
        }

        deserializer.deserialize_str(Visitor)
    }
}
