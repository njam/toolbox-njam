use std::collections::HashSet;
use std::env;
use std::path::{Path, PathBuf};

use anyhow::{anyhow, Context};
use fs_err as fs;
use itertools::Itertools;
use regex::Regex;
use users::os::unix::UserExt;

use crate::container_commands::*;
use crate::podman::*;
use crate::toolbox::*;
use crate::util::current_user::*;
use crate::util::env_opt::*;
use crate::util::fs::*;

/// Manage toolbox containers with podman
pub struct ToolboxContainers {
    podman: Podman,
    host_env: ToolboxHostEnv,
}

impl ToolboxContainers {
    pub fn new() -> Result<Self, anyhow::Error> {
        Ok(Self {
            podman: Podman::new(),
            host_env: ToolboxHostEnv::new()?,
        })
    }

    pub fn create(&self, definition: &ToolboxDefinition) -> Result<ToolboxPair, anyhow::Error> {
        if self.find(&definition.name)?.is_some() {
            return Err(anyhow!("Toolbox with name '{}' already exists.", definition.name));
        };
        let container = self.create_container(definition)?;
        let pair = ToolboxPair { definition: definition.clone(), container };
        self.setup_container(&pair)?;
        Ok(pair)
    }

    pub fn attach_shell(&self, pair: &ToolboxPair) -> Result<(), anyhow::Error> {
        if !pair.container.state_running {
            self.podman.start(PodmanStart {
                container: pair.container.container_id.clone(),
            })?;
        }
        let command = PodmanCommand {
            cmd: "/usr/bin/toolbox-njam".into(),
            args: vec!(
                "internal".into(),
                "start-shell".into(),
                format!("--payload={}", serde_json::to_string(&CommandInternalStartShell {
                    shell_path: pair.definition.shell.clone(),
                })?),
            ),
        };
        self.exec_replace(pair, command)?;
        Ok(())
    }

    pub fn exec(&self, pair: &ToolboxPair, cmd: String, args: Vec<String>) -> Result<(), anyhow::Error> {
        if !pair.container.state_running {
            self.podman.start(PodmanStart {
                container: pair.container.container_id.clone(),
            })?;
        }
        let command = PodmanCommand { cmd, args };
        self.exec_replace(pair, command)?;
        Ok(())
    }

    pub fn list(&self) -> Result<Vec<ToolboxContainer>, anyhow::Error> {
        self.list_by_filter("label=toolbox-njam=true".into())
    }

    pub fn delete(&self, container: &ToolboxContainer) -> Result<(), anyhow::Error> {
        self.podman.rm(PodmanRemove {
            container: container.container_id.clone(),
            force: true,
        }).context("Cannot remove container")?;
        Ok(())
    }

    pub fn find(&self, name: &str) -> Result<Option<ToolboxContainer>, anyhow::Error> {
        let mut items = self.list_by_filter(format!("name=^{}$", name))?;
        match items.len() {
            0 | 1 => {
                Ok(items.pop())
            }
            count => Err(anyhow!("Unexpected: more than 1 ({}) containers with name '{}'", count, name)),
        }
    }

    fn list_by_filter(&self, filter: String) -> Result<Vec<ToolboxContainer>, anyhow::Error> {
        let defs = self.podman
            .list_containers(PodmanList {
                all: true,
                filter: Some(filter),
            })?
            .into_iter()
            .map(ToolboxContainer::from_podman_inspect)
            .sorted_by_key(|c| c.name.clone())
            .collect();
        Ok(defs)
    }

    fn exec_replace(&self, pair: &ToolboxPair, command: PodmanCommand) -> Result<(), anyhow::Error> {
        self.podman.exec_replace(PodmanExec {
            container: pair.container.container_id.clone(),
            interactive: true,
            tty: true,
            user: self.host_env.user_name.clone(),
            workdir: pair.definition.workdir.clone(),
            command,
        })?;
        Ok(())
    }

    fn setup_container(&self, pair: &ToolboxPair) -> Result<(), anyhow::Error> {
        let mounts = pair.definition.mounts.iter()
            .map(PathFileOrDir::path_buf)
            .collect();
        let payload = CommandInternalSetupContainer {
            user_uid: self.host_env.user_uid,
            user_gid: self.host_env.user_gid,
            user_name: self.host_env.user_name.clone(),
            home_dir: self.host_env.home.clone(),
            shell_path: pair.definition.shell.clone(),
            toolbox_name: pair.definition.name.clone(),
            mounts,
        };
        self.podman.exec(PodmanExec {
            container: pair.container.container_id.clone(),
            interactive: false,
            tty: false,
            user: "root".to_string(),
            workdir: None,
            command: PodmanCommand {
                cmd: "/usr/bin/toolbox-njam".into(),
                args: vec!(
                    "internal".into(),
                    "setup-container".into(),
                    format!("--payload={}", serde_json::to_string(&payload)?),
                ),
            },
        })?;
        Ok(())
    }

    fn create_container(&self, def: &ToolboxDefinition) -> Result<ToolboxContainer, anyhow::Error> {
        let mut envs = Vec::new();
        let mut security_opt = HashSet::new();
        let mut cap_add = HashSet::new();
        let mut devices = HashSet::new();
        self.check_and_create_user_mounts(def)?;
        let mut mounts = self.get_mounts(def);
        if let Some(xdg_runtime_dir) = &self.host_env.xdg_runtime_dir {
            envs.push(PodmanEnvVar::new("XDG_RUNTIME_DIR", xdg_runtime_dir.to_string_lossy()))
        }
        if let Some(dbus_session_bus_address) = &self.host_env.dbus_session_bus_address {
            envs.push(PodmanEnvVar::new("DBUS_SESSION_BUS_ADDRESS", dbus_session_bus_address.clone()))
        }
        if let Some(wayland_display) = &self.host_env.wayland_display {
            envs.push(PodmanEnvVar::new("WAYLAND_DISPLAY", wayland_display.clone()))
        }
        if let Some(xorg_display_num) = self.host_env.xorg_display_num {
            envs.push(PodmanEnvVar::new("DISPLAY", format!(":{}", xorg_display_num)))
        }
        if def.enable_nested_podman {
            // See https://github.com/containers/podman/issues/4056#issuecomment-683285209
            security_opt.insert("label=disable".into());
            security_opt.insert("seccomp=unconfined".into());
            cap_add.insert("SYS_ADMIN".into());
            devices.insert("/dev/fuse".into());
            let host_dir_containers = self.host_env.create_data_dir(&Path::new(&def.name).join("containers"))?;
            mounts.push(PodmanMount::Bind {
                source: host_dir_containers,
                destination: PathBuf::from("/var/lib/containers"),
                read_only: false,
            });
        }
        // Disable SELinux labeling so we can access files mounted from the host
        security_opt.insert("label=disable".into());
        let user_ns = match self.host_env.user_uid {
            0 => PodmanUserNs::Root,
            _ => PodmanUserNs::KeepId,
        };
        let container_id = self.podman.run_detach(PodmanRunDetach {
            image: def.image.clone(),
            name: def.name.clone(),
            user: "root".to_string(),
            user_ns,
            security_opt,
            privileged: true,
            cap_add,
            labels: vec!(
                ("toolbox-njam".into(), "true".into()),
            ),
            mounts,
            devices,
            envs,
            command: PodmanCommand {
                cmd: "/usr/bin/toolbox-njam".into(),
                args: vec!(
                    "internal".into(),
                    "run-pid1".into(),
                ),
            },
        })?;
        let inspect = self.podman.inspect_container(PodmanInspectContainer {
            container: container_id,
        })?;
        let container = ToolboxContainer::from_podman_inspect(inspect);
        Ok(container)
    }

    fn get_mounts(&self, def: &ToolboxDefinition) -> Vec<PodmanMount> {
        let mut default_mounts = vec!(
            PodmanMount::Bind {
                source: self.host_env.current_exe.clone(),
                destination: PathBuf::from("/usr/bin/toolbox-njam"),
                read_only: true,
            },
            // Mount the full `/dev` as a workaround for https://github.com/containers/podman/issues/4900
            // See https://github.com/containers/podman/issues/4900#issuecomment-604437546
            PodmanMount::Bind {
                source: PathBuf::from("/dev"),
                destination: PathBuf::from("/dev"),
                read_only: false,
            },
            // If we mount `/dev` we need a `/dev/pts` separate from the host-system
            PodmanMount::DevPts {
                destination: PathBuf::from("/dev/pts"),
            },
            // Mount a tmpfs to `/tmp`, to make sure the directory is cleared every once and then.
            PodmanMount::Tmpfs {
                destination: PathBuf::from("/tmp"),
            }
        );
        if let Some(xdg_runtime_dir) = &self.host_env.xdg_runtime_dir {
            default_mounts.push(PodmanMount::Bind {
                source: xdg_runtime_dir.clone(),
                destination: xdg_runtime_dir.clone(),
                read_only: false,
            });
        }
        if let Some(dbus_system_bus_socket) = &self.host_env.dbus_system_bus_socket {
            default_mounts.push(PodmanMount::Bind {
                source: dbus_system_bus_socket.clone(),
                destination: dbus_system_bus_socket.clone(),
                read_only: false,
            });
        }
        if let Some(xorg_socket_folder) = &self.host_env.xorg_socket_folder {
            default_mounts.push(PodmanMount::Bind {
                source: xorg_socket_folder.clone(),
                destination: xorg_socket_folder.clone(),
                read_only: false,
            });
        }
        let user_mounts = def.mounts.iter()
            .map(|path| {
                PodmanMount::Bind {
                    source: path.path_buf(),
                    destination: path.path_buf(),
                    read_only: false,
                }
            })
            .collect::<Vec<_>>();
        itertools::concat(vec!(default_mounts, user_mounts))
    }

    fn check_and_create_user_mounts(&self, def: &ToolboxDefinition) -> Result<(), anyhow::Error> {
        for path in &def.mounts {
            if !path.exists() {
                if path.is_file() {
                    return Err(anyhow!("Mounted file does not exist on host: {}", path));
                } else {
                    if let Some(directory) = path.first_directory() {
                        log::info!("Creating mounted directory on host: {}", directory.display());
                        fs::create_dir_all(directory)?;
                    }
                }
            }
        }
        Ok(())
    }
}

#[derive(Debug)]
pub struct ToolboxContainer {
    pub name: String,
    pub image: String,
    pub container_id: PodmanContainerId,
    pub state_running: bool,
}

impl ToolboxContainer {
    fn from_podman_inspect(inspect: PodmanInspectReturn) -> Self {
        ToolboxContainer {
            name: inspect.name,
            image: inspect.image_name,
            container_id: inspect.id,
            state_running: inspect.state.running,
        }
    }
}

pub struct ToolboxHostEnv {
    #[allow(unused)]
    current_dir: PathBuf,
    current_exe: PathBuf,
    xdg_dirs: xdg::BaseDirectories,
    xdg_runtime_dir: Option<PathBuf>,
    xorg_display_num: Option<u64>,
    xorg_socket_folder: Option<PathBuf>,
    wayland_display: Option<String>,
    dbus_session_bus_address: Option<String>,
    dbus_system_bus_socket: Option<PathBuf>,
    home: PathBuf,
    user_uid: u32,
    user_gid: u32,
    user_name: String,
}

impl ToolboxHostEnv {
    fn new() -> Result<Self, anyhow::Error> {
        let current_dir = env::current_dir()?;
        let current_exe = env::current_exe()?;
        let xdg_dirs = xdg::BaseDirectories::with_prefix("toolbox-njam")?;
        let xdg_runtime_dir = env_opt("XDG_RUNTIME_DIR")?.map(PathBuf::from);
        let xorg_display_num = Self::detect_xorg_display()?;
        let xorg_socket_folder = {
            let path = PathBuf::from("/tmp/.X11-unix");
            if path.exists() { Some(path) } else { None }
        };
        let wayland_display = env_opt("WAYLAND_DISPLAY")?;
        let dbus_session_bus_address = env_opt("DBUS_SESSION_BUS_ADDRESS")?;
        let dbus_system_bus_socket = {
            let path = PathBuf::from("/var/run/dbus/system_bus_socket");
            if path.exists() { Some(path) } else { None }
        };
        let user = get_current_user()?;
        Ok(Self {
            current_dir,
            current_exe,
            xdg_runtime_dir,
            xdg_dirs,
            xorg_display_num,
            xorg_socket_folder,
            wayland_display,
            dbus_session_bus_address,
            dbus_system_bus_socket,
            home: user.home_dir().to_path_buf(),
            user_uid: user.uid(),
            user_gid: user.primary_group_id(),
            user_name: user.name().to_string_lossy().to_string(),
        })
    }

    fn detect_xorg_display() -> Result<Option<u64>, anyhow::Error> {
        let display_number = match env_opt("DISPLAY")? {
            Some(value) => {
                let re = Regex::new(r"^:(\d+)$").unwrap();
                let number = re.captures(&value)
                    .with_context(|| format!("Cannot extract Xorg display number from '{}'", &value))?
                    .get(1).unwrap().as_str()
                    .parse()
                    .with_context(|| format!("Cannot parse Xorg display number from '{}'", &value))?;
                Some(number)
            }
            None => None,
        };
        Ok(display_number)
    }

    fn create_data_dir(&self, path: &Path) -> Result<PathBuf, anyhow::Error> {
        let path = self.xdg_dirs.create_data_directory(path)?;
        Ok(path)
    }
}
