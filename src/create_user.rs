use std::os::unix::fs::PermissionsExt;
use std::path::{Path, PathBuf};

use anyhow::Context;
use fs_err as fs;
use itertools::Itertools;
use regex::RegexBuilder;
use walkdir::WalkDir;
use crate::util::fs::*;

/// Update `/etc/group` to include the given group
/// See https://linux.die.net/man/5/group
pub fn ensure_group(group_gid: u32, group_name: &str, user_name: &str) -> Result<(), anyhow::Error> {
    let path = "/etc/group";
    let content = fs::read_to_string(path)?;
    // Remove existing entries with the given name or id
    let search_by_name = RegexBuilder::new(&format!("^{}:(?P<password>.*?):(?P<gid>.*?):(?P<user_list>.*?)$", regex::escape(group_name)))
        .multi_line(true)
        .build()?;
    let search_by_gid = RegexBuilder::new(&format!("^(?P<group_name>.*?):(?P<password>.*?):{}:(?P<user_list>.*?)$", regex::escape(&group_gid.to_string())))
        .multi_line(true)
        .build()?;
    let mut lines = content
        .lines()
        .filter(|&line| !search_by_name.is_match(line) && !search_by_gid.is_match(line))
        .collect::<Vec<_>>();
    // Add the new entry
    let entry = format!(
        "{}:x:{}:{}",
        group_name,
        group_gid,
        user_name,
    );
    lines.push(&entry);
    fs::write(path, format!("{}\n", lines.join("\n")))?;
    Ok(())
}

/// Update `/etc/passwd` to include the given user
/// See https://linux.die.net/man/5/passwd
pub fn ensure_user(user_name: &str, user_uid: u32, user_gid: u32, home_dir: &str, shell: &str) -> Result<(), anyhow::Error> {
    let path = "/etc/passwd";
    let content = fs::read_to_string(path)?;
    // Remove existing entries with the given name or id
    let search_by_name = RegexBuilder::new(&format!("^{}:(?P<password>.*?):(?P<uid>.*?):(?P<gid>.*?):(?P<gecos>.*?):(?P<home>.*?):(?P<shell>.*?)$", regex::escape(user_name)))
        .multi_line(true)
        .build()?;
    let search_by_gid = RegexBuilder::new(&format!("^(?P<name>.*?):(?P<password>.*?):{}:(?P<gid>.*?):(?P<gecos>.*?):(?P<home>.*?):(?P<shell>.*?)$", regex::escape(&user_uid.to_string())))
        .multi_line(true)
        .build()?;
    let mut lines = content
        .lines()
        .filter(|&line| !search_by_name.is_match(line) && !search_by_gid.is_match(line))
        .collect::<Vec<_>>();
    // Add the new entry
    let entry = format!(
        "{}:*:{}:{}::{}:{}",
        user_name,
        user_uid,
        user_gid,
        home_dir,
        shell,
    );
    lines.push(&entry);
    fs::write(path, format!("{}\n", lines.join("\n")))?;
    Ok(())
}

/// Configure password-less sudo for the given user
pub fn configure_sudo(user_name: &str) -> Result<(), anyhow::Error> {
    let sudo_config_folder = PathBuf::from("/etc/sudoers.d");
    if sudo_config_folder.exists() {
        let sudo_config = sudo_config_folder.join(user_name);
        if !&sudo_config.exists() {
            fs::write(&sudo_config, format!("{} ALL=(ALL) NOPASSWD: ALL\n", user_name))
                .context(format!("Cannot write sudo config to '{}'", sudo_config.to_string_lossy()))?;
        }
    }
    Ok(())
}

/// Create home-directory, copy `/etc/skel` into it
pub fn ensure_home(home_dir: &Path, user_uid: u32, user_gid: u32, mounts: &Vec<PathBuf>) -> Result<(), anyhow::Error> {
    // Create home dir
    fs::create_dir_all(home_dir)?;
    fs_chown(home_dir, user_uid, user_gid)?;

    // Copy skel
    let skel = PathBuf::from("/etc/skel");
    if skel.exists() {
        copy_skel(&skel, home_dir, user_uid, user_gid, mounts)?;
    }
    Ok(())
}


/// Copy a `/etc/skel` tree to a home-directory, and set ownership accordingly.
/// Paths below any of the given `mounts` are ignored (to avoid copying skel-files into mounted folders).
fn copy_skel(source: &Path, target: &Path, user_uid: u32, user_gid: u32, mounts: &Vec<PathBuf>) -> Result<(), anyhow::Error> {

    let walker = WalkDir::new(source).min_depth(1);
    for entry in walker {
        let entry = entry
            .context(format!("Cannot read an entry in '{}'", source.display()))?;
        let source_item = entry.path();
        let target_item = target.join(source_item.strip_prefix(source)?);
        let target_is_in_mount = mounts.iter().any(|m| target_item.starts_with(m));
        if !target_is_in_mount && !target_item.exists() {
            // Create target
            if source_item.is_dir() {
                std::fs::create_dir(&target_item)?;
            } else if entry.path_is_symlink() {
                std::os::unix::fs::symlink(fs::read_link(source_item)?, &target_item)?;
            } else {
                std::fs::copy(source_item, &target_item)?;
            }
            // Set ownership
            fs_chown(&target_item, user_uid, user_gid)?;
            // `fchmodat` fails for symlinks with "EOPNOTSUPP". Not sure why, for now we just leave symlinks as-is
            if !entry.path_is_symlink() {
                let source_metadata = fs::symlink_metadata(source_item)
                    .context(format!("Cannot get metadata of source path '{}'", source_item.display()))?;
                let source_mode = source_metadata.permissions().mode();
                fs_chmod(&target_item, source_mode)?;
            }
        }
    }
    Ok(())
}

/// Fix ownership for sub-folders of the home-directory.
///
/// When a toolbox mounts `~/sub1/file1` into a container, then the folder `~/sub1/` will be
/// created if it doesn't exist, and will be owned by "root". But most likely this folder should be
/// owned by the user in who's home-directory it is.
pub fn fix_home_sub_dirs(home_dir: &Path, user_uid: u32, user_gid: u32, mounts: &Vec<PathBuf>) -> Result<(), anyhow::Error> {
    let folders_inside_home = mounts.iter()
        // Find any mounts that are sub-folders of the home-directory
        .filter(|&mount| mount.starts_with(home_dir))
        // For each mount, extract ancestor-components below the home-directory
        .flat_map(|mount| {
            mount.ancestors()
                .skip(1)
                .filter(|&ancestor| ancestor.starts_with(home_dir))
                .map(|p| p.to_path_buf())
        })
        .dedup()
        .collect::<Vec<_>>();
    for folder in folders_inside_home {
        nix::unistd::chown(
            &folder.to_path_buf(),
            Some(nix::unistd::Uid::from_raw(user_uid)),
            Some(nix::unistd::Gid::from_raw(user_gid)),
        ).context(format!("Failed to change ownership of '{}' to '{}'", home_dir.display(), user_uid))?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use users::get_current_uid;
    use std::os::linux::fs::MetadataExt;

    use crate::command::run_command;

    use super::*;

    #[test]
    fn test_copy_skel() {
        let source = tempfile::TempDir::new().unwrap();
        let target = tempfile::TempDir::new().unwrap();
        let user_id = get_current_uid();
        // Skel-files inside mounted folders are not copied, so any files inside `/dir4/` are ignored.
        let mounts = vec!(
            target.path().join("dir4"),
        );

        // Build source
        run_command("sh", &["-c", &format!(r#"
            mkdir -p {source}/dir1
            mkdir -p {source}/dir2/dir3
            mkdir -p {source}/dir4
            echo "jo" > {source}/file1 &&
            echo "jo" > {source}/.file2 &&
            echo "jo" > {source}/dir1/file3 &&
            echo "jo" > {source}/dir2/dir3/file4
            echo "jo" > {source}/dir4/file5
            ln -s ./dir1/file3 {source}/link1
            chmod 0600 {source}/.file2
            chmod 0700 {source}/dir2
        "#, source = source.path().display())]).unwrap();

        // Copy
        copy_skel(source.path(), target.path(), user_id, user_id, &mounts).unwrap();

        // Check target
        assert_eq!(print_file_tree(target.path()), vec!(
            format!("100600 {user_id}:{user_id} .file2 3B", user_id = user_id),
            format!(" 40755 {user_id}:{user_id} dir1", user_id = user_id),
            format!("100644 {user_id}:{user_id} dir1/file3 3B", user_id = user_id),
            format!(" 40700 {user_id}:{user_id} dir2", user_id = user_id),
            format!(" 40755 {user_id}:{user_id} dir2/dir3", user_id = user_id),
            format!("100644 {user_id}:{user_id} dir2/dir3/file4 3B", user_id = user_id),
            format!("100644 {user_id}:{user_id} file1 3B", user_id = user_id),
            format!("120777 {user_id}:{user_id} link1 -> ./dir1/file3", user_id = user_id),
        ).join("\n"));
    }

    fn print_file_tree(folder: &Path) -> String {
        WalkDir::new(folder).min_depth(1).into_iter()
            .sorted_by_key(|result| {
                result.as_ref().map_or(Default::default(), |entry| {
                    entry.path().display().to_string()
                })
            })
            .map(|entry| {
                let entry = entry.unwrap();
                let path = entry.path();
                let path_relative = path.strip_prefix(folder).unwrap();
                let metadata = fs::symlink_metadata(entry.path()).unwrap();
                let uid: u32 = metadata.st_uid();
                let gid: u32 = metadata.st_gid();
                let mode: u32 = metadata.permissions().mode();
                let prefix = format!("{:>6o} {}:{} {}", mode, uid, gid, path_relative.display());
                if entry.path_is_symlink() {
                    let target = std::fs::read_link(path).unwrap();
                    format!("{} -> {}", prefix, target.display())
                } else if metadata.is_file() {
                    format!("{} {}B", prefix, metadata.len())
                } else {
                    prefix
                }
            })
            .join("\n")
    }
}
