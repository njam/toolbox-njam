use std::ffi::OsStr;
use std::process::Command;

use log::debug;

use anyhow::anyhow;

pub fn run_command<P, A, AS>(program: P, args: A) -> Result<String, anyhow::Error>
    where
        P: AsRef<OsStr>,
        A: IntoIterator<Item=AS> + Clone,
        AS: AsRef<OsStr>,
{
    let command_debug = format_command_debug(&program, &args);
    debug!("Running: {}", command_debug);
    let output = Command::new(program).args(args).output()?;
    let stdout = String::from_utf8_lossy(&output.stdout);
    if !output.status.success() {
        let stderr = String::from_utf8_lossy(&output.stderr);
        return Err(anyhow!("Command failed:\n{}\n\n{}\n\n{}\n", command_debug, stdout, stderr));
    }
    Ok(stdout.into())
}

pub fn exec_command<P, A, AS>(program: P, args: A) -> anyhow::Error
    where
        P: AsRef<OsStr>,
        A: IntoIterator<Item=AS> + Clone,
        AS: AsRef<OsStr>,
{
    let command_debug = format_command_debug(&program, &args);
    debug!("Executing: {}", command_debug);
    let args_vec: Vec<_> = args.into_iter().collect();
    let error = exec::Command::new(program).args(args_vec.as_slice()).exec();
    error.into()
}

fn format_command_debug<P, A, AS>(program: &P, args: &A) -> String
    where
        P: AsRef<OsStr>,
        A: IntoIterator<Item=AS> + Clone,
        AS: AsRef<OsStr>,
{
    let program_str = program.as_ref().to_string_lossy().to_string();
    let args_str = args.clone().into_iter()
        .map(|arg| arg.as_ref().to_string_lossy().to_string())
        .collect::<Vec<String>>()
        .join(" ");
    format!("{} {}", program_str, args_str)
}
