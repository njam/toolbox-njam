toolbox-njam
============

Experimental Rust-clone of Fedora's [toolbox](https://github.com/containers/toolbox) for managing Linux containers for software development.

Installation
------------

### Pre-built Binaries
Pre-built binaries for Linux are available for download in the [releases](https://gitlab.com/njam/toolbox-njam/-/releases) section.

### From Source Code

Make sure the musl toolchain is installed:
```
rustup target add x86_64-unknown-linux-musl
```

Then install the latest version from git:
```
cargo install --target 'x86_64-unknown-linux-musl' --git 'https://gitlab.com/njam/toolbox-njam'
```

Usage
-----

### Configure toolbox containers

Create a config file `~/.config/toolbox-njam.toml` and configure the desired containers:
```toml
[toolbox.arch]
image = "registry.gitlab.com/njam/toolbox-njam/arch-toolbox"
shell = "/bin/zsh"
workdir = "~/Projects"
mounts = [
  "~/Projects",
  "~/.ssh",
]

[toolbox.debian]
image = "docker.io/library/debian:10"
shell = "/bin/bash"
workdir = "~/Projects"
mounts = [
  "~/Projects",
]
```

Options per toolbox are:

| Name                  | Type      | Optional? | Description       |
| ----                  | ---       | ---       | ---               |
| image                 | string    | mandatory | The container image/URL. |
| shell                 | string    | optional  | Path to the shell to run inside the container when using the `shell` command. Defaults to the user's shell configured in the container. |
| workdir               | string    | optional  | Working directory inside the container. Defaults to the container's working directory. |
| mounts                | string[]  | optional  | List of directories on the host to mount into the container. |
| privileged            | bool      | optional  | Run container as "privileged", meaning it has the same access to devices as the user launching the container. Defaults to `false`. |
| enable_nested_podman  | bool      | optional  | Enable running _podman_ containers inside the container. This adds the "SYS_ADMIN" capability amongst other things. Defaults to `false`. |
 

### List all toolbox containers

```console
$ toolbox-njam list

NAME    IMAGE                                               STATUS
arch    registry.gitlab.com/njam/toolbox-njam/arch-toolbox  Not Created
debian  docker.io/library/debian:10                         Not Created
```

### Start a Shell in a toolbox container

The `shell` command starts a shell in the toolbox and attaches the terminal to it.

```console
$ toolbox-njam shell arch

Creating toolbox 'arch'...
[user@6a6934ec6e0c Projects]$
```

### Execute a Command in a toolbox container

The `exec` command runs a command in a toolbox container

```console
$ toolbox-njam exec arch head -n1 /etc/os-release
NAME="Arch Linux"
```


Development
-----------

To build the project, make sure to install the "musl" target: 
```
rustup target add x86_64-unknown-linux-musl
```

Now you can run the tests:
```
cargo test
```

### Container Image

This repo also builds a container image based on _Arch Linux_, that can be used as a toolbox image.
The image URL is `registry.gitlab.com/njam/toolbox-njam/arch-toolbox`.

To build the Arch-Linux image manually:
```
podman build --layers --tag arch-toolbox ./images/arch-toolbox/latest/
```



Releasing a new Version
-----------------------

1. If needed, install [cargo-release](https://github.com/sunng87/cargo-release): `cargo install cargo-release`
2. Check out the master branch
3. Publish a new tag: `cargo release [patch|minor|major]`
