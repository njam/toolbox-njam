FROM docker.io/library/alpine:3

RUN mkdir -p /etc/skel/.cache/toolbox-njam-test/mounted/ &&\
    echo "File1: from skel" >/etc/skel/.cache/toolbox-njam-test/file1 &&\
    echo "File2: from skel" >/etc/skel/.cache/toolbox-njam-test/mounted/file2
