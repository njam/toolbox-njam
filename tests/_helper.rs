use std::path::Path;
use std::sync::{Mutex, MutexGuard};

use assert_cmd::Command;
use fs_err as fs;
use once_cell::sync::OnceCell;

/// Combine stdout+stderr into a single String for test assertions
pub fn combined_output(assert: &assert_cmd::assert::Assert) -> String {
    let stdout = std::str::from_utf8(&assert.get_output().stdout).unwrap();
    let stderr = std::str::from_utf8(&assert.get_output().stderr).unwrap();
    let output = match (stdout.is_empty(), stderr.is_empty()) {
        (true, false) => stderr.to_string(),
        (false, true) => stdout.to_string(),
        (false, false) => format!("{}\n---\n{}", stdout, stderr),
        (true, true) => "".to_string(),
    };
    output.replace("\r\n", "\n")
}

/// Delete known toolboxes created in tests
pub fn cleanup_toolboxes() {
    // As defined in "tests/fixtures/*.toml"
    // We could parse the output of "list" instead.
    let toolboxes = [
        "integration_test_arch",
        "integration_test_debian",
        "integration_test_alpine",
        "integration_test_podman",
        "integration_test",
    ];
    for toolbox in &toolboxes {
        Command::cargo_bin("toolbox-njam").unwrap()
            .args(["--config=tests/fixtures/config-empty.toml", "delete", "--if-exists", toolbox])
            .assert().success();
    }
}

/// Global lock to ensure integration tests run in sequence
pub fn sequential_run_lock() -> MutexGuard<'static, ()> {
    static LOCK: OnceCell<Mutex<()>> = OnceCell::new();
    let lock = LOCK.get_or_init(Default::default);
    lock.lock()
        // Ignore poisoned mutex caused by the previous test failing
        .unwrap_or_else(|e| e.into_inner())
}

/// Build a container image from a "alpine-with-skel.Dockerfile".
pub fn build_container_image<D>(dockerfile: D, image_name: &str)
    where
        D: AsRef<Path>,
{
    let dockerfile_path: &Path = dockerfile.as_ref();
    Command::new("podman")
        .args([
            "build",
            &format!("--tag={}", image_name),
            &format!("--file={}", dockerfile_path.to_string_lossy()),
        ])
        .assert()
        .success();
}

/// Remove a directory and all its contents, if it exists
pub fn remove_dir_if_exists<P>(path: P) -> Result<(), anyhow::Error>
    where
        P: AsRef<Path>,
{
    let path: &Path = path.as_ref();
    if path.exists() {
        fs::remove_dir_all(path)?;
    }
    Ok(())
}
