use assert_cmd::Command;
use fs_err as fs;
use insta::assert_snapshot;
use users::{get_current_uid, get_user_by_uid};
use users::os::unix::UserExt;

use crate::_helper::*;

mod _helper;

#[test]
fn list() {
    let _lock = sequential_run_lock();
    cleanup_toolboxes();

    let assert = Command::cargo_bin("toolbox-njam").unwrap()
        .args([
            "--config=tests/fixtures/config-list.toml",
            "list",
        ])
        .assert()
        .success();
    let expected = "\
        NAME                     IMAGE                                               STATUS\n\
        integration_test_arch    registry.gitlab.com/njam/toolbox-njam/arch-toolbox  Not Created\n\
        integration_test_debian  docker.io/library/debian:10                         Not Created\n\
    ";
    assert_eq!(combined_output(&assert), expected);

    cleanup_toolboxes();
}

#[test]
fn exec() {
    let _lock = sequential_run_lock();
    cleanup_toolboxes();

    // First run will create toolbox container
    let assert = Command::cargo_bin("toolbox-njam").unwrap()
        .args([
            "--config=tests/fixtures/config-alpine.toml",
            "exec", "integration_test",
            "head", "-n1", "/etc/os-release",
        ])
        .assert()
        .success();
    assert_snapshot!(combined_output(&assert), @r###"
        Creating toolbox 'integration_test'...
        NAME="Alpine Linux"
    "###);

    // Second run will re-use existing container
    let assert = Command::cargo_bin("toolbox-njam").unwrap()
        .args([
            "--config=tests/fixtures/config-alpine.toml",
            "exec", "integration_test",
            "head", "-n1", "/etc/os-release",
        ])
        .assert()
        .success();
    assert_snapshot!(combined_output(&assert), @r###"
        NAME="Alpine Linux"
    "###);

    cleanup_toolboxes();
}

#[test]
fn exec_alpine() {
    let _lock = sequential_run_lock();
    cleanup_toolboxes();

    let assert = Command::cargo_bin("toolbox-njam").unwrap()
        .args([
            "--config=tests/fixtures/config-alpine.toml", "--quiet",
            "exec", "integration_test",
            "head", "-n1", "/etc/os-release",
        ])
        .assert()
        .success();
    assert_snapshot!(combined_output(&assert), @r###"
        NAME="Alpine Linux"
    "###);

    cleanup_toolboxes();
}

#[test]
fn exec_debian() {
    let _lock = sequential_run_lock();
    cleanup_toolboxes();

    let assert = Command::cargo_bin("toolbox-njam").unwrap()
        .args([
            "--config=tests/fixtures/config-debian.toml", "--quiet",
            "exec", "integration_test",
            "head", "-n1", "/etc/os-release",
        ])
        .assert()
        .success();
    assert_snapshot!(combined_output(&assert), @r###"
        PRETTY_NAME="Debian GNU/Linux 10 (buster)"
    "###);

    cleanup_toolboxes();
}

#[test]
fn exec_arch() {
    let _lock = sequential_run_lock();
    cleanup_toolboxes();

    let assert = Command::cargo_bin("toolbox-njam").unwrap()
        .args([
            "--config=tests/fixtures/config-arch.toml", "--quiet",
            "exec", "integration_test",
            "head", "-n1", "/etc/os-release",
        ])
        .assert()
        .success();
    assert_snapshot!(combined_output(&assert), @r###"
        NAME="Arch Linux"
    "###);

    cleanup_toolboxes();
}

#[test]
fn exec_nested_podman() {
    let _lock = sequential_run_lock();
    cleanup_toolboxes();

    let assert = Command::cargo_bin("toolbox-njam").unwrap()
        .args([
            "--config=tests/fixtures/config-nested-podman.toml", "--quiet",
            "exec", "integration_test_podman",
            "sudo", "podman", "run", "-q", "docker.io/library/alpine:3", "head", "-n1", "/etc/os-release",
        ])
        .assert()
        .success();
    assert_snapshot!(combined_output(&assert), @r###"
        NAME="Alpine Linux"
    "###);

    cleanup_toolboxes();
}


/// Verify that a path inside the user's home folder (like `~/sub1/file1`) has its components
/// (e.g. `~/sub1`) owned by the user.
#[test]
fn home_sub_mounts() {
    let _lock = sequential_run_lock();
    cleanup_toolboxes();

    let user_id = get_current_uid();
    let user = get_user_by_uid(user_id).unwrap();
    let user_home = user.home_dir();
    remove_dir_if_exists(user_home.join(".cache/toolbox-njam-test/")).unwrap();
    fs::create_dir_all(user_home.join(".cache/toolbox-njam-test/sub1/")).unwrap();
    fs::write(user_home.join(".cache/toolbox-njam-test/sub1/file1"), "Test").unwrap();

    let assert = Command::cargo_bin("toolbox-njam").unwrap()
        .args([
            "--config=tests/fixtures/config-home-sub-mounts.toml", "--quiet",
            "exec", "integration_test", "sh", "-c",
            "stat -c '%u:%g' ~/.cache/toolbox-njam-test/sub1",
        ])
        .assert()
        .success();
    assert_eq!(combined_output(&assert), format!("\
        {}:{}\n\
    ", user_id, user_id));

    cleanup_toolboxes();
}

/// Verify that mounted directories are created on the host
#[test]
fn mounts_create_dirs() {
    let _lock = sequential_run_lock();
    cleanup_toolboxes();

    let user_id = get_current_uid();
    let user = get_user_by_uid(user_id).unwrap();
    let user_home = user.home_dir();
    remove_dir_if_exists(user_home.join(".cache/toolbox-njam-test/")).unwrap();

    let assert = Command::cargo_bin("toolbox-njam").unwrap()
        .args([
            "--config=tests/fixtures/config-mounts-create-dirs.toml", "--quiet",
            "exec", "integration_test", "sh", "-c",
            "stat -c '%u:%g' ~/.cache/toolbox-njam-test/sub1/dir",
        ])
        .assert()
        .success();
    assert_eq!(combined_output(&assert), format!("\
        {}:{}\n\
    ", user_id, user_id));

    cleanup_toolboxes();
}


/// Verify that contents of `/etc/skel` are copied when the user is created in a new toolbox.
///
/// In this test the 'skel' contains two files:
/// - `~/.cache/toolbox-njam-test/file1`
/// - `~/.cache/toolbox-njam-test/mounted/file2`
///
/// Because the toolbox is mounting `~/.cache/toolbox-njam-test/mounted/` from the host, only
/// the first file should be copied from 'skel', while the second file should not.
#[test]
fn skel() {
    let _lock = sequential_run_lock();
    cleanup_toolboxes();

    build_container_image(
        "./tests/fixtures/container_images/alpine-with-skel.Dockerfile",
        "toolbox_njam--integration_test--skel",
    );

    let user_id = get_current_uid();
    let user = get_user_by_uid(user_id).unwrap();
    let user_home = user.home_dir();
    remove_dir_if_exists(user_home.join(".cache/toolbox-njam-test/")).unwrap();
    fs::create_dir_all(user_home.join(".cache/toolbox-njam-test/mounted/")).unwrap();

    let assert = Command::cargo_bin("toolbox-njam").unwrap()
        .args([
            "--config=tests/fixtures/config-skel.toml", "--quiet",
            "exec", "integration_test", "sh", "-c", r#"
                cd ~/.cache/toolbox-njam-test/ &&
                find . -print -exec stat -c "%u:%g" {} \;
            "#])
        .assert()
        .success();
    assert_eq!(combined_output(&assert), format!("\
        .\n\
        {user_id}:{user_id}\n\
        ./mounted\n\
        {user_id}:{user_id}\n\
        ./file1\n\
        {user_id}:{user_id}\n\
    ", user_id = user_id));

    cleanup_toolboxes();
}
